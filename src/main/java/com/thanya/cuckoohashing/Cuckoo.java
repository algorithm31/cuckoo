/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.cuckoohashing;

/**
 *
 * @author Thanya
 */
public class Cuckoo {

    int m = 11;
    int nTable = 2;

     Node[][] table = new Node[nTable][m];

    public Cuckoo(int size) {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < nTable; j++) {
                table[j][i] = null;
            }
        }
    }

    class Node {
        int key;
        String value;
        Node(int key, String value) {
            this.key = key;
            this.value = value;
        }
    }
    
    private int getHashIndex(int table, int key) {
        switch (table) {
            case 1:
                return key % m;
            case 2:
                return (key / m) % m;
        }
        return 0;
    }
    
    public void put(int key, String value) {
        if (table[0][getHashIndex(1, key)] != null && table[0][getHashIndex(1, key)].key == key) {
            table[0][getHashIndex(1, key)].value = value;
            return;
        }
        if (table[1][getHashIndex(2, key)] != null && table[1][getHashIndex(2, key)].key == key) {
            table[1][getHashIndex(2, key)].value = value;
            return;
        }
    }
   
    public  String get(int key) {
        if(table[0][getHashIndex(1, key)] != null && table[0][getHashIndex(1, key)].key == key) {
            return table[0][getHashIndex(1, key)].value;
        }
        if(table[1][getHashIndex(2, key)] != null && table[1][getHashIndex(2, key)].key == key) {
            return table[1][getHashIndex(2, key)].value;
        }
        return null;
    }
    
    public void remove(int key) {
        if(table[0][getHashIndex(1, key)] != null && table[0][getHashIndex(1, key)].key == key) {
            table[0][getHashIndex(1, key)] = null;
        }
        if(table[1][getHashIndex(2, key)] != null && table[1][getHashIndex(2, key)].key == key) {
            table[1][getHashIndex(2, key)] = null;
        }
    }
    
    public void printTable() {
        for (int i = 1; i < nTable+1; i++) {
            for(int j = 0; j < m; j++) {
                if(table[i - 1][j] != null) {
                    System.out.println("["+i+", "+j+"] = "+table[i-1][j].key+", "+table[i - 1][j].value);
                } else {
                    System.out.println("["+i+", "+j+"] = is null");
                }
                System.out.println("");
            }
        }
    }
}
